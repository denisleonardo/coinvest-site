@extends('layouts.default')

@section('bg-nav')
    <link type="text/css" rel="stylesheet" href="{{asset('css/nav.css')}}" />
@endsection
@section('title')
<title>Fale Conosco | Coinvest</title>
@endsection
@section('content')
<section id="title" class="contact">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
            </div>
        </div>
    </div>
</section>
<section id="contact-content" class="mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="contact-content-shadow font-blue">
                    <div class="row">
                        <div class="col-md-4 my-auto">
                            <h2 class="font-dark-gold font-normal detail-title">
                                AINDA COM <br />
                                DÚVIDAS?<br />
                                ENTRE EM<br />
                                CONTATO.<br />
                            </h2>
                        </div>
                        <div class="col-md-8 my-auto text-center">
                            <form id="contact">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="nome" placeholder="Nome*"
                                                required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" placeholder="E-mail*"
                                                required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="telefone"
                                                placeholder="Telefone*" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="4"
                                                placeholder="Envie-nos uma mensagem..."></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 text-right">
                                        <input type="submit" class="btn btn-md btn-info" value="Enviar" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection