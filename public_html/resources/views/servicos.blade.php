@extends('layouts.default')

@section('bg-nav')
    <link type="text/css" rel="stylesheet" href="{{asset('css/nav.css')}}" />
@endsection
@section('title')
<title>Serviços | Coinvest</title>
@endsection
@section('content')
<section id="title" class="services">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
            </div>
        </div>
    </div>
</section>
<section id="services-content" class="mb-5">
    <div class="container mb-5">
        <div class="row">
            <div class="col-md-12 mx-auto my-auto">
                <div class="h1 font-blue font-medium title-serv position-absolute ">
                    SERVIÇOS
                </div>
            </div>
            <div id="recebiveis" class="container-fluid limited">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-12 col-12 p-0">
                            <div class="box mt-4 d-flex">
                                <div class="h2 text-center mt-4 mx-auto font-medium gold mb-5">
                                    RECEBÍVEIS <br /> CONVENCIONAIS
                                </div>
                                <div class="bord"></div>
                                <div class="img">
                                    <img src="{{asset('img/servicos/recebiveis_3.png')}}">
                                    <a href="{{route('comoFunciona')}}#serv-1" class="text-decoration-none"><div class="h4 text-center">Saiba mais...</div></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-12 p-0">
                            <div class="box mt-4 d-flex">
                                <div class="h2 text-center mt-4 mx-auto font-medium gold">
                                    FOMENTO <br /> À PRODUÇÃO
                                </div>
                                <div class="bord"></div>
                                <div class="img">
                                    <img src="{{asset('img/servicos/fomento_1.png')}}">
                                    <a href="{{route('comoFunciona')}}#serv-2" class="text-decoration-none"><div class="h4 text-center">Saiba mais...</div></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-12 p-0">
                            <div class="box mt-4 d-flex">
                                <div class="h2 mt-4 mx-auto font-medium gold">
                                    TRUSTEE
                                </div>
                                <div class="bord"></div>
                                <div class="img">
                                    <img src="{{asset('img/servicos/trustee_2.png')}}">
                                    <a href="{{route('comoFunciona')}}#serv-3" class="text-decoration-none"><div class="h4 text-center">Saiba mais...</div></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-4 col-sm-12 col-12 p-0">
                            <div class="box mt-4 d-flex">
                                <div class="h2 font-medium gold text-center mt-4 mx-auto">
                                    SERVIÇOS DE <br /> COBRANÇA
                                </div>
                                <div class="bord"></div>
                                <div class="img">
                                    <img src="{{asset('img/servicos/cobranca_1.png')}}">
                                    <a href="{{route('comoFunciona')}}#serv-4" class="text-decoration-none"><div class="h4 text-center">Saiba mais...</div></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-12 p-0">
                            <div class="box mt-4 d-flex">
                                <div class="h2 font-medium gold mt-4 text-center mx-auto">
                                    CONSULTORIA<br /> E GESTÃO
                                </div>
                                <div class="bord"></div>
                                <div class="img">
                                    <img src="{{asset('img/servicos/cobranca_4.png')}}">
                                    <a href="{{route('comoFunciona')}}#serv-5" class="text-decoration-none"><div class="h4 text-center">Saiba mais...</div></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-12 p-0">
                            <div class="box d-flex mt-4">
                                <div class="h2 font-medium gold mt-4 text-center mx-auto">
                                    OPERAÇÕES<br />ESTRTURADAS
                            </div>
                            <div class="bord"></div>
                            <div class="img">
                                <img src="{{asset('')}}" alt="">
                                <a href="{{route('comoFunciona')}}#serv-6" class="text-decoration-none"><div class="h4 text-center">Saiba mais...</div></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
</section>
@endsection