@extends('layouts.default')

@section('title')
<title>Coinvest</title>
@endsection
@section('content')
<section id="banner" class="h-100">
    <div id="carouselExampleFade" class="carousel slide carousel-fade h-100" data-ride="carousel">
        <div class="carousel-inner h-100">
          <div class="carousel-item h-100 active">
            <div id="htmlcaption" class="animated fadeInRight position-absolute">
                <div class="h2 font-gold font-thin caption">Fluxos comerciais <br /> sem atraso <br /> e interrupções</div>
                <div class="divider-slider"></div>
                <a href="{{ route('contato') }}" class="btn-style1 mt-5">FALE COM A COINVEST</a>
            </div>
            <img src="{{ asset('img/banners/b1.jpg') }}" class="img-fluid h-100 w-100" />
          </div>
          <div class="carousel-item h-100">
            <div id="htmlcaption1" class="animated fadeInRight position-absolute">
                <div class="h2 font-gold font-thin caption">Crédito rápido<br/>e descomplicado<br/>para a sua empresa.</div>
                <div class="divider-slider"></div>
            </div>
            <img src="{{ asset('img/banners/b2.jpg') }}" class="img-fluid h-100 w-100" />
          </div>
          <div class="carousel-item h-100">
            <div id="htmlcaption2" class="animated fadeInRight position-absolute">
                <div class="h2 font-gold font-thin caption">Solidez financeira<br/>previsibilidade de fluxo de caixa.</div>
                <div class="divider-slider"></div>
                <a href="{{ route('servicos') }}" class="btn-style1 mt-5">SAIBA COMO</a>
            </div>
            <img src="{{ asset('img/banners/b3.jpg') }}" class="img-fluid h-100 w-100" />
        </div>
        <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
</section>
<section id="chave">
    <div class="container-fluid h-100">
        <div class="row h-100">
            <div class="col-sm-6 my-auto">
                <div class="chave-bloco">
                    <h2 class="font-dark-gold font-thin">SOLUÇÃO FINANCEIRAS <br/> - A CHAVE<br /> DO  NOSSO NEGÓCIO.</h2>
                    <div class="divider"></div>
                    <h3 class="font-blue font-thin">Somos um fundo de <br /> investimento em direitos <br />
                        creditórios. (FIDC)
                    </h3>
                    <br />
                    <a href="{{ route('servicos') }}" class="btn-style1">SAIBA MAIS</a>
                </div>
            </div>
            <div class="col-sm-6 px-0">
                <img src="{{ asset('img/chave/right.jpg') }}" class="img-fluid h-100 w-100" />
            </div>
        </div>
    </div>
</section>
<section id="credito">
    <div class="container-fluid">
        <div class="row">
            <div class="ml-auto my-auto col-md-7">
                <div class="credito-block text-right">
                    <div class="credito-center">
                        <h2 class="font-thin font-gold">Não OFERECEMOS apenas crédito, <br /> oferecemos apoio ao CRESCIMENTO.</h2>
                        <br />
                        <h4 class="font-thin font-white">Fale com a Coinvest para <br /> o seu negócio prosperar.</h4>
                        <br />
                        <a href="{{ route('servicos') }}" class="btn-style1">SAIBA MAIS</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="faturamento">
    <div class="container-fluid h-100">
        <div class="row h-100">
            <div class="ml-auto col-md-5">
                <div class="row h-100">
                    <div class="faturamento-block text-right">
                        <img src="{{ asset('img/faturamento/right.jpg') }}" class="img-fluid h-100 w-100" />
                        <div class="faturamento-center">
                            <h2 class="font-thin font-blue">Segurança nas tomadas de decisões.
                            </h2>
                            <div class="divider-right"></div>
                            <h4 class="font-thin font-dark-gold clearfix">Aproveite as oportunidades de crescimento.
                            </h4>
                            <br />
                            <a href="{{ route('servicos') }}" class="btn-style1">QUERO</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="negocio">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 negocio-colbg">
                <div class="row">
                    <div class="negocio-block">
                        <h3 class="font-thin font-gold">Você focado<br /> no que realmente importa: </h3>
                        <h1 class="font-bold font-gold">O seu negócio!</h1>
                        <br />
                        <a href="{{ route('servicos') }}" class="btn-style1">COMO?</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">

            </div>
        </div>
    </div>
</section>
<section id="solucao_financeira">
    <div class="container">
        {{-- <div class="row d-none d-sm-block">
            <div class="col-md-12">
                <h2 class="font-white font-thin text-center">A solução financeira que o seu negócio <br />
                    precisa, você tem com a gente.</h2>
            </div>
        </div>
        <div class="row d-block d-sm-none">
            <div class="col-md-12">
                <h2 class="font-white font-thin text-center">A solução financeira que o seu negócio
                    precisa,<br /> você tem com a gente.</h2>
            </div>
        </div> --}}
        <div class="row">
            <div class="col-md-3">
                <div class="bloco">
                    <img src="{{ asset('img/industria.png') }}" alt="">
                    <p>Indústria</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="bloco">
                    <img src="{{ asset('img/agricola.png') }}" alt="">
                    <p>Agrícola</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="bloco">
                    <img src="{{ asset('img/comercial.png') }}" alt="">
                    <p>Comercial</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="bloco">
                    <img src="{{ asset('img/servico.png') }}" alt="">
                    <p>Serviço</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('nav-js')
    <script src="{{asset('js/nav-js.js')}}"></script>
@endsection