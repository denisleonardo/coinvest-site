<li class="nav-item {{ (\Request::route()->getName() == 'empresa') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('empresa') }}">EMPRESA</a>
</li>
<li class="nav-item {{ (\Request::route()->getName() == 'servicos') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('servicos') }}">SERVIÇOS</a>
</li>
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">
        COMO FUNCIONA
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{route('comoFunciona')}}#serv-1">Recebíveis Convencional</a>
        <a class="dropdown-item" href="{{route('comoFunciona')}}#serv-2">Fomento à produção</a>
        <a class="dropdown-item" href="{{route('comoFunciona')}}#serv-3">Trustee</a>
        <a class="dropdown-item" href="{{route('comoFunciona')}}#serv-4">Serviço de Cobrança</a>
        <a class="dropdown-item" href="{{route('comoFunciona')}}#serv-5">Consultoria e gestão</a>
    </div>
</li>

<li class="nav-item {{ (\Request::route()->getName() == 'faq') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('faq') }}">F.A.Q.</a>
</li>
<li class="nav-item {{ (\Request::route()->getName() == 'contato') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('contato') }}">FALE CONOSCO</a>
</li>