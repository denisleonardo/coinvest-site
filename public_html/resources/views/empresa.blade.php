@extends('layouts.default')

@section('bg-nav')
    <link type="text/css" rel="stylesheet" href="{{asset('css/nav.css')}}" />
@endsection
@section('title')
<title>Empresa | Coinvest</title>
@endsection
@section('content')
<section id="title" class="about">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
            </div>
        </div>
    </div>
</section>
<section id="about-content" class="mb-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 my-auto text-center">
                <h1 class="font-dark-gold detail-title">A COINVEST</h1>
            </div>
            <div class="col-md-8">
                <div class="col-md-12">
                    <div class="about-content-shadow font-blue">
                        <div class="h5 font-thin mb-5">Somos a Coinvest, empresa atuante como FIDC (Fundo de Investimento em Direitos Creditórios).
                            Nossa equipe é formada por profissionais com grande experiência no mercado de crédito para pequenas empresas. E é essa equipe que vai trabalhar junto com você para o seu crescimento.
                        </div>
                        <div class="h5 font-thin mb-5">
                            Nossa missão é:
                            <li>financiar seu desenvolvimento</li>
                            <li>ajudá-lo no descasamento do fluxo de caixa</li>
                            <li>contribuir com melhorias na gestão financeira</li>
                        </div>
                        <div class="h5 font-thin mb-5">
                            Tudo isso com:
                            <li>Relacionamento</li>
                            <li>Agilidade</li>
                            <li>Transparência</li>
                            <li>Normas do COAF</li>
                            <li>Obediência às exigências legais</li>
                        </div>
                        <div class="h5">Fale com o Coinvest.</div>
                        <div class="h5">Seu negócio com soluções financeiras à vista.</div>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection