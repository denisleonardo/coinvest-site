<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="h3 font-thin" style="color:#b2955f">
                    EMPRESAS QUE CONFIAM <br /> NA <span class="font-medium kkkk ">COINVEST</span>
                </div>
                <div class="empre d-block">
                    <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                               <img class="img-fluid my-md-0 my-sm-3 my-4" src="{{asset('img/empresas/wba.jpg')}}">
                            </div>
                            <div class="carousel-item">
                                <img class="img-fluid my-md-0 my-sm-3 my-4" src="{{asset('img/empresas/crdc.jpg')}}">
                            </div>
                            <div class="carousel-item">
                                <img class="img-fluid my-md-0 my-sm-3 my-4" src="{{asset('img/empresas/bradesco.jpg')}}">
                            </div>
                            <div class="carousel-item">
                                <img class="img-fluid my-md-0 my-sm-3 my-4" src="{{asset('img/empresas/idTrust.jpg')}}">
                            </div>
                            <div class="carousel-item">
                                <img class="img-fluid my-md-0 my-sm-3 my-4" src="{{asset('img/empresas/itau.jpg')}}">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>            
                </div>
            </div>
            <div class="col-md-6 my-auto ml-auto">
                <img src="{{ asset('img/logo-white.png') }}" class="d-none d-sm-block logo" style="margin-bottom: 40px;">
                <img src="{{ asset('img/logo-white.png') }}" alt="" class="d-block d-sm-none mx-auto logo" style="margin-bottom: 40px;">
                <ul class="info-bottom ml-auto">
                    <li class="align-middle">
                        <img src="{{ asset('img/icons/icon_local.png') }}" alt="" class="pull-left" style="height:35px">
                        <div class="h6">R. Eduardo Chaves 168, São Paulo / SP</div>
                    </li>
                    <li class="align-middle">
                        <img src="{{ asset('img/icons/icon_contact.png') }}" alt="" class="pull-left"
                            style="height:35px">
                        <div class="h6">+55 11 3228 0417 / +55 11 3228 0445 <br/> +55 11 3329 9434 / +55 11 4200 0831 </div>
                    </li>
                    <li class="align-middle">
                        <img src="{{ asset('img/icons/icon_mail.png') }}" alt="" class="pull-left" style="height:35px">
                        <div class="h6">contato@coinvestcapital.com.br</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<div class="copyright">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center font-cp">
                Coinvest Capital@2020 - Todos os direitos reservados | Desenvolvido por <a
                    href="https://agencialed.com.br" class="font-white">LED Digital</a>
            </div>
        </div>
    </div>
</div>