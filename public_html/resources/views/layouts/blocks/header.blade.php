<nav id="navtarget" class="navbar navbar-expand-lg navbar-light fixed-top {{ Request::is('/') ? '' : '' }}">
    <div class="container">
        <a class="navbar-brand" href="{{ route('home') }}">
            <img src="{{ asset('img/logo-white.png') }}" alt="" class="img-fluid logo">
        </a>
        <button class="navbar-toggler custom-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                @include('blocks.menu')
            </ul>
        </div>
    </div>
</nav>