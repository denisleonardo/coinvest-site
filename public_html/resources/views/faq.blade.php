@extends('layouts.default')

@section('bg-nav')
    <link type="text/css" rel="stylesheet" href="{{asset('css/nav.css')}}" />
@endsection
@section('title')
<title>F.A.Q. | Coinvest</title>
@endsection
@section('content')
<section id="title" class="faq mb-5">
    <div class="container-fluid h-100 ">
        <div class="row h-100">
            <div class="col-sm-6  mx-auto">
                <div class="title-bottom">
                    <h1 class="font-blue font-medium detail-title2">FAQ</h1>
                    <h1 class="font-blue font-normal">PERGUNTAS <br /> FREQUENTES</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="faq-content" class="mb-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <div id="accordion" class="accordion">
                    <div class="card">
                        <div class="card-header collapsed" data-toggle="collapse" href="#question1">
                            <a class="card-title font-gold h5" href="javascript:;"> 1. O que é desconto de duplicatas?</a>
                        </div>
                        <div id="question1" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                O desconto de duplicatas é uma operação financeira onde as duplicatas, chamadas de
                                recebíveis são antecipadas para o recebimento a vista de um valor que seria parcelado.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header collapsed" data-toggle="collapse" href="#question2">
                            <a class="card-title font-gold h5" href="javascript:;"> 2. O que é desconto de cheques?</a>
                        </div>

                        <div id="question2" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                O desconto ou antecipação de cheques pré-datados é realizada no processo de compra dos
                                recebíveis onde o formato de pagamento escolhido foi o cheque.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header collapsed" data-toggle="collapse" href="#question3">
                            <a class="card-title font-gold h5" href="javascript:;"> 3. Crédito Rápido</a>
                        </div>

                        <div id="question3" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                O crédito rápido permite a ingestão de recursos financeiros imediatos para sua empresa,
                                seja para pagar débitos como investir em novas oportunidades, equipamentos ou pessoas.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header collapsed" data-toggle="collapse" href="#question4">
                            <a class="card-title font-gold h5" href="javascript:;"> 4. O que é antecipação de recebíveis?</a>
                        </div>

                        <div id="question4" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <p>Antecipação de recebíveis é forma de gerar recursos líquidos imediatos para a
                                    empresa utilizando os recebíveis.</p>

                                <p>Vamos supor que uma empresa de prestação de serviços firma um contrato com seu
                                    cliente e irá receber de forma parcelada durante 2 anos. Nesta situação os
                                    recebíveis são as parcelas pré-datadas que podem ter sido acordadas como duplicadas,
                                    cartão de crédito, carnês ou mesmo cheques. Ao invés a prestadora de serviços
                                    receber o valor mês a mês, ela pode antecipar estes recebíveis com uma empresa de
                                    factoring (fomento mercantil) como a Coinvest Capital e gerar caixa imediato.</p>

                                <p>A Coinvest Capital gera caixa imediato no mesmo dia da aprovação e o processo é
                                    muito simples, basta nos enviar o XML da nota fiscal da operação para análise. Nosso
                                    atendimento também pode ser pessoal, em horário comercial nossos especialistas
                                    retornam o contato em até uma hora.</p>

                                <p>O dinheiro que você receberia no futuro se torna caixa presente. A factoring também
                                    assume a responsabilidade total pelo recebimento dos títulos negociados, ou seja,
                                    não há também dor de cabeça de cobrança ou inadimplência.</p>

                                <p>O principal fator determinante na antecipação de recebíveis é a taxa da transação,
                                    pois você receberá a vista o valor total dos títulos – taxa da factoring, por isso é
                                    preciso se planejar para que o dinheiro a vista seja usado de forma estratégica e
                                    lucrativa.</p>

                                <p>Este tipo de operação é totalmente livre de cobrança de juros, já que a empresa
                                    está utilizando os ativos da própria empresa como recurso de antecipação.</p>

                                <p>De forma simplificada, essa operação é como antecipar décimo terceiro ou Imposto de
                                    Renda para gerar dinheiro imediato, porém, a transação é realizada entre empresas.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header collapsed" data-toggle="collapse" href="#question5">
                            <a class="card-title font-gold h5" href="javascript:;">5. Como funciona a antecipação de recebíveis?</a>
                        </div>

                        <div id="question5" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <p>Devido a diversos fatores como a dificuldade em aquisição de linhas de créditos e
                                    seus processos burocráticos, as atividades de antecipação de recebíveis têm se
                                    tornado a cada dia mais comum entre os empresários como alternativa de gerar
                                    liquidez para sua empresa.</p>
                                <p>A antecipação de recebíveis acontece de forma muito simples, como na Coinvest
                                    Capital, por exemplo, onde todo o processo é realizado de forma rápida e totalmente
                                    digital.</p>

                                <p>Para entender como funciona a antecipação de recebíveis, primeiro precisamos saber
                                    quem são os envolvidos:</p>
                                <ul>
                                    <li>1) Empresa que vai vender e gerar o título,
                                        no caso, você cliente da Coinvest Capital</li>
                                    <li>2) Empresa de Fomento Mercantil que irá
                                        antecipar o título, nós da Coinvest Capital</li>
                                    <li>3) Devedor, cliente que irá pagar os
                                        recebíveis</li>
                                </ul>
                                <p>A empresa gera uma venda parcelada e tem em posse a nota fiscal, contrato, cheques
                                    pré-datados e duplicadas. Ao invés de receber parceladamente, você entra em contato
                                    com a Coinvest Capital, a qual irá solicitar o XML da Nota Fiscal da operação para
                                    validar a antecipação. Com o ok da validação, no mesmo dia você recebe a vista todo
                                    o dinheiro que iria receber a prazo. A Coinvest Capital possui uma das melhores
                                    taxas do mercado, além de atendimento irrepreensível na tratativa com seu cliente.
                                </p>

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header collapsed" data-toggle="collapse" href="#question6">
                            <a class="card-title font-gold h5" href="javascript:;"> 6. Antecipação de recebíveis em bancos</a>
                        </div>

                        <div id="question6" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <p>A antecipação de recebíveis é uma prática muito utilizada por bancos, os quais
                                    dominam o cenário e geraram um contexto de altas taxas que dificultam a vida do
                                    pequeno empresário na aquisição de créditos.</p>

                                <p>Entre as principais práticas, os bancos fazem vários tipos de antecipação, como de
                                    imposto de renda, décimo terceiro, pagamentos via cartão de crédito, entre outros. É
                                    preciso estar atento à taxa cobrada, pois, a facilidade para se realizar a
                                    antecipação pode iludir, principalmente quando se utiliza termos como vantagens, sem
                                    juros e crédito instantâneo.</p>

                                <p>Não é diferente com os recebíveis, separamos algumas diferenças entre bancos e
                                    factoring para você entender melhor como ambos trabalham:</p>

                                <ul>
                                    <li>Linhas de Créditos: Os bancos são intermediários de crédito enquanto as
                                        factoring não fazem operações de crédito. A factoring compra o bem móvel e
                                        termina sua operação enquanto o banco faz uma operação passiva, como tomador de
                                        recursos.</li>
                                    <li>Captação de recursos: Os bancos captam recursos no mercado e empresta, assim
                                        como intermediação de recursos de terceiros, como uso da poupança. A Factoring
                                        não capta recursos ou administra recursos de terceiros.</li>
                                    <li>Juros: O banco recebe juros, ou seja, remuneração pelo uso do dinheiro durante
                                        um espaço de tempo. A factoring coloca um preço nos recebíveis, ou seja,
                                        remuneração da compra de crédito.</li>
                                </ul>
                                <p>Impostos: O banco gera impostos sobre operações financeiras, imposto de renda e
                                    demais enquanto a factoring paga ISS sobre serviço, imposto de renda, imposto sobre
                                    operações financeiras e mais contribuições.</p>

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header collapsed" data-toggle="collapse" href="#question7">
                            <a class="card-title font-gold h5" href="javascript:;"> 7. O que é securitização de recebíveis?</a>
                        </div>

                        <div id="question7" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <p>A securitização de recebíveis é a conversão de recebíveis, chamados de
                                    créditos, em títulos ou valores mobiliários emitidos posteriormente.</p>

                                <p>A securitização de recebíveis é a conversão de recebíveis, chamados de
                                    créditos, em títulos ou valores mobiliários emitidos posteriormente.
                                    Securitização de ativos empresariais são sociedades de propósito específico
                                    (SPE) com o objetivo exclusivo da aquisição e securitização de ativos
                                    empresarias naturais de operações praticadas pela indústria, comércio ou
                                    prestadora de serviços.</p>

                                <p>A securitização de recebíveis consiste especificamente na aquisição e
                                    securitização de recebíveis empresariais e colocação dos mesmos no ambiente
                                    privado de valores mobiliários, podendo emitir outros títulos e créditos e
                                    realizar negócios pertinentes a securitização.</p>

                                <p>Assim como o Factoring, a securitização é uma operação financeira que visa
                                    gerar recursos imediatos ao caixa da empresa, gerando benefícios como: aumentar
                                    a capacidade produtiva; maior poder de compra; aumento do capital de giro.</p>
                                <p>Os títulos gerados pela securitização de recebíveis são vendidas a
                                    investidores, como este mercado têm se tornado mais sofisticado e os recebíveis
                                    podem ser dos mais variados, a busca de compra desses títulos só tem aumentado.
                                </p>
                                <p>A securitização de recebíveis possui 3 partes, a saber:</p>

                                <ul>
                                    <li>Gerador do título: empresa que realiza a venda e gera o recebível</li>
                                    <li>Intermediário: Aquele cuja atribuição é estruturar a operação de securitização
                                        e viabilizar a distribuição entre os investidores, no caso, a Coinvest Capital.
                                    </li>
                                    <li>Investidores: Compradores dos títulos</li>
                                </ul>
                            </div>
                            <div><br></div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header collapsed" data-toggle="collapse" href="#question8">
                            <a class="card-title font-gold h5" href="javascript:;"> 8. Taxas</a>
                        </div>

                        <div id="question8" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <p>De forma totalmente simplificada, a taxa na antecipação de recebíveis é o valor
                                    cobrado pela operação onde transforma o valor a receber e seu risco em dinheiro
                                    imediato.</p>
                                <p>Exemplo simplificado de antecipação de recebíveis:</p>

                                <ul>
                                    <li>1) Sua empresa possui 24 boletos a serem recebidos em 2 anos</li>
                                    <li>2) A Coinvest Capital analisa o cliente que irá pagar os boletos e gera a melhor
                                        taxa de mercado para antecipar esse valor</li>
                                    <li>3) Sua empresa recebe a vista o valor que recebia em 2 anos descontando a taxa.
                                    </li>
                                </ul>

                                <p>A antecipação de recebíveis não é apenas uma prática para a empresa que passa por
                                    dificuldades financeiras, é também uma oportunidade de gerar liquidez imediata para
                                    comprar novos equipamentos, investir em pessoal ou aproveitar uma oportunidade.
                                </p>
                                <p>A taxa de transação não é uma taxa de juros, como a antecipação de recebíveis gera
                                    dinheiro proveniente de recursos (ativos) da empresa, não há nenhum tipo de cobrança
                                    de juro.</p>
                                <p>Vale ressaltar também que a taxa está totalmente associada ao devedor (quem irá
                                    pagar os títulos), pois, a financeira irá arcar com todos os riscos de não
                                    recebimento, ou seja, não haverá o perigo de receber parcialmente o valor, mesmo em
                                    caso de não pagamento do devedor.</p>

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header collapsed" data-toggle="collapse" href="#question9">
                            <a class="card-title font-gold h5" href="javascript:;">9. Vantagens</a>
                        </div>

                        <div id="question9" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <p>A antecipação de recebíveis pode ser uma operação muito lucrativa em determinadas
                                    oportunidades.</p>

                                <p><b>Fluxo de Caixa</b></p>
                                <p>O aumento do fluxo de caixa facilita o planejamento de ações que podem promover o
                                    negócio e sua vantagem competitiva.</p>

                                <p><b>Poder de Compra</b></p>
                                <p>O dinheiro a vista pode gerar grandes descontos com fornecedores na compra à vista,
                                    ou seja, você antecipa os recebíveis por uma taxa menor que o desconto e lucra com a
                                    compra à vista.</p>

                                <p><b>Saída de emergência</b></p>
                                <p>Em caso de emergências, o acesso ao dinheiro de forma imediata pode dar um fôlego
                                    extra no caixa para momentos mais delicados.</p>

                                <p><b>Velocidade</b></p>
                                <p>O processo de antecipação de recebíveis com a Coinvest Capital, ao contrário dos
                                    bancos tradicionais, é rápido e descomplicado. Não há burocracias.</p>
                                <p><b><br></b></p>
                                <p><b>Não há juros</b></p>
                                <p>Não há dívidas ou juros. O pagamento pelos recebíveis é imediato. A Coinvest
                                    Capital irá comprar os títulos.</p>

                                <p><b>Taxa baixíssima</b></p>
                                <p>Obtenção de menores taxas. Como o dinheiro à vista permite pagamentos de dívidas,
                                    normalmente as taxas são menores que os juros das dívidas.</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection