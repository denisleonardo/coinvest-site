@extends('layouts.default')

@section('bg-nav')
    <link type="text/css" rel="stylesheet" href="{{asset('css/nav.css')}}" />
@endsection
@section('estilo')
    <link rel="stylesheet" href="{{ asset('css/services.css')}}">
@endsection
@section('title')
    <title>Como Funciona | Coinvest</title>
@endsection
@section('content')
<section id="servicos">
    <div class="container-fluid" id="top">

    </div>
  <div class="container">
      <div class="row">
          <div class="col-md-12">
              <div class="row" id="text-contente">
                  <div class="col-md-6">
                      <div class="text-center">
                          <div class="display-3 my-4 title-section">Serviços</div>
                          <div class="bord-title"></div>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="text-infos">A <span>Coinvest</span> oferece soluções para financiar o crescimento das
                          empresas. Desde o apoio e suporte para o seu negócio buscar desafios maiores, ser mais agressivo
                          comercialmente, ou até investir em novos projetos. <span>É sua empresa com liquidez para operar.</span>
                        </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="container mt-3">
      <div class="row">
          <div class="col-md-12 px-0 content">
              <div class="servico-um mt-2">
                  <div class="con-tex d-flex">
                      <div class="box-text mx-auto mt-4" id="serv-1">
                            <div class="row d-flex">
                                <div class="col-md-6">
                                    <div class="ml-md-5 ml-sm-4 ml-3 box-title d-flex">
                                        <div class="title mt-5">RECEBÍVEIS<br />CONVENCIONAIS</div>
                                    </div>
                                    <div class="bords ml-0"></div>
                                    <div class="text-content h6 p-5">
                                      Uma vez que o título ou recebido está em propriedade da Fomentadora ela entrará em
                                      contato
                                      com o Devedor para informar sobre a cobrança. A dívida do devedor é quitada com a
                                      Fomentadora, ou seja, a empresa não tem o risco ou responsabilidade pelo recebimento.
                                    </div>
                                </div>
                                <div class="col-md-6 my-auto">
                                    <div class="title-2 mt-5 h5">VEJA COMO<br/>FUNCIONA</div>
                                    <div class="text-content mt-4 pr-5 h6">Após o primeiro contato, é aberto um cadastro e realizada uma análise de crédito;A empresa envia a nota fiscal eletrônica para a Coinvest e a análise desta operação é feita.<br/> Após a aprovação, o valor contratado estará disponível.
                                        <br/>A emissão dos boletos de cobrança referentes ao valor negociado são feitos. Seu cliente estará bem assistido conosco.
                                    </div>
                                </div>
                            </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-12 my-4 p-0">
              <div class="border-service">
                  <div class="bord-interna mx-auto"></div>
              </div>
          </div>
          <div class="col-md-12 p-0 content">
              <div class="servico-um">
                  <div class="con-tex d-flex">
                      <div class="box-text mx-auto" id="serv-2">
                            <div class="row d-flex">
                                <div class="col-md-6">
                                    <div class="ml-md-5 ml-sm-4 ml-3 box-title d-flex">
                                        <div class="title mt-5">FOMENTO<br />À PRODUÇÃO</div>
                                    </div>
                                    <div class="bords ml-0"></div>
                                    <div class="text-content h5 p-5">
                                      Atuando como intermediário entre cliente e fornecedores, é realizado o pagamento à vista
                                      para o fornecedor, o que garante competitividade e preços especiais. O fator nos lucros
                                      da
                                      transformação da matéria-prima é acertado e o pagamento do fomento está relacionado às
                                      vendas dos lotes em questão.
                                    </div>
                                </div>
                                <div class="col-md-6 my-auto">
                                    <div class="title-2 mt-5 h5">VEJA COMO<br/>FUNCIONA</div>
                                    <div class="text-content mt-4 pr-5 h6">
                                        Antecipar recursos para uma empresa financiar a sua produção, como insumos, matérias-primas etc.
                                        <br/>Depois o valor retorna como recebível.
                                    </div>
                                </div>
                            </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-12 my-4 p-0">
              <div class="border-service">
                  <div class="bord-interna mx-auto"></div>
              </div>
          </div>
          <div class="col-md-12 p-0 content">
              <div class="servico-um">
                  <div class="con-tex d-flex">
                      <div class="box-text mx-auto" id="serv-3">
                        <div class="ml-md-5 ml-sm-4 ml-3 box-title d-flex">
                            <div class="title mt-5">TRUSTEE</div>
                        </div>
                        <div class="bords ml-0"></div>
                          <div class="text-content h5 p-5">
                              Atuando como gestora financeira, a <strong>Coinvest</strong> administra as contas a
                              receber
                              e também é
                              estabelecido o contrato de fomento mercantil.
                              O cliente pode atuar em sua área enquanto a <strong>Coinvest</strong> faz a
                              administração
                              total de
                              recebíveis.
                            </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-12 my-4 p-0">
              <div class="border-service">
                  <div class="bord-interna mx-auto"></div>
              </div>
          </div>
          <div class="col-md-12 p-0 content">
              <div class="servico-um">
                  <div class="con-tex d-flex">
                      <div class="box-text mx-auto" id="serv-4">
                            <div class="row d-flex">
                                <div class="col-md-6">
                                    <div class="ml-md-5 ml-sm-4 ml-3 box-title d-flex">
                                        <div class="title mt-5"> SERVIÇOS DE <br /> COBRANÇA</div>
                                    </div>
                                    <div class="bords ml-0"></div>
                                      <div class="text-content h5 p-5">
                                          Os serviços de cobrança da Coinvest têm o objetivo de aumentar a taxa de recebimentos de
                                          seus clientes terceirizando esse processo para nós.
                                      </div>
                                </div>
                                <div class="col-md-6 my-auto">
                                    <div class="title-2 mt-5 h5">VEJA COMO<br/>FUNCIONA</div>
                                    <div class="text-content mt-4 pr-5 h6">
                                        Apresentação dos serviços e taxas entre a Coinvest e o Cliente.
                                        <br/>Coletamos os recebíveis e organizamos os processos para realizar o atendimento.
                                        <br/>Realização do atendimento com tratativa totalmente profissional.
                                        <br/>Elaboração de relatórios com o resultado do trabalho para o cliente.
                                    </div>
                                </div>
                            </div>
                      </div>
                  </div>
              </div>
          </div>
        <div class="col-md-12 my-4 p-0">
            <div class="border-service">
                <div class="bord-interna mx-auto"></div>
            </div>
        </div>
        <div class="col-md-12 p-0 mb-4 content">
            <div class="servico-um">
                <div class="con-tex d-flex">
                    <div class="box-text mx-auto" id="serv-5">
                        <div class="ml-md-5 ml-sm-4 ml-3 box-title d-flex">
                            <div class="title mt-5"> CONSULTORIA<br />E GESTÃO</div>
                        </div>
                        <div class="bords ml-0"></div>
                        <div class="text-content h5 p-5">
                            A consultoria e gestão da Coinvest tem foco administrativo e financeiro. É realizada minuciosa análise
                            para encontrar os pontos e dificuldades da empresa afim de otimizar os processos financeiros. O
                            principal objetivo é a redução de custos e aumento de controle para tomada de decisões mais
                            assertivas. A gestão financeira é o conjunto de estratégias e ações que compreendem o departamento
                            financeiro da empresa.
                        </div>
                    </div>
                </div>
            </div>
          </div>
      </div>
  </div>
</section>
@endsection