<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'NavigationController@index')->name('home');
Route::get('/empresa', 'NavigationController@empresa')->name('empresa');
Route::get('/servicos', 'NavigationController@servicos')->name('servicos');
Route::get('/faq', 'NavigationController@faq')->name('faq');
Route::get('/fale-conosco', 'NavigationController@contato')->name('contato');
Route::get('/como-funciona', 'NavigationController@comoFunciona')->name('comoFunciona');
