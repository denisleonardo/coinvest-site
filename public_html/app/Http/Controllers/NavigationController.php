<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NavigationController extends Controller
{

    public function comoFunciona(Request $request){
        return view('como-funciona');
    }

    public function index(Request $request)
    {
        return view('index');
    }

    public function empresa(Request $request)
    {
        return view('empresa');
    }
    public function servicos(Request $request)
    {
        return view('servicos');
    }
    public function faq(Request $request)
    {
        return view('faq');
    }

    public function contato(Request $request)
    {
        return view('contato');
    }


}
